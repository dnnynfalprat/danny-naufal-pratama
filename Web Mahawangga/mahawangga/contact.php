<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>


    <body style="background-color:#F0F8FF;">
    

      <nav class="navbar navbar-expand-lg navbar-light  fixed-top" id="mainNav">
        <div class="container">
        <a class="navbar-brand" href="test.html"><b> MAHAWANGGA</b></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      
        </button>
      
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="test.html"><b> HOME </b><span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><b>Team Building Activities</b></a>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="virtual.html">Virtual</a>
                <a class="dropdown-item" href="hybird.html">Hybrid</a>
                <a class="dropdown-item" href="person.html">In-Person</a>
              </div>
            </li>
            
            <li class="nav-item">
              <a class="nav-link" href="profile.html"><b> About Us</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="contact.html"><b> Contact Us</b></a>
            </li>
          </ul>
        
        </div>
      </div>
      </nav>

          <br><br>
    <div class="container" style="margin:1cm;">
        <h1>Contact Us</h1>
        <img src="undraw_contact_us_15o2.svg" class="d-block w-50" style="float: right;" >
        <form action="kirim.php" method="post">
            <p><input type="text" name="nama" placeholder="Nama anda" size="50" required /></p>
            <p><input type="email" name="email" placeholder="Email valid" size="50" required /></p>
            <p><input type="text" name="judul" placeholder="Subjek pesan email" size="50" required /></p>
            <p><textarea name="pesan" placeholder="Pesan anda" rows="10" cols="70" required></textarea>
            <p><input type="submit" name="kirim" value="Kirim Pesan" class="btn btn-primary" /> <input type="reset" value="Hapus Text" class="btn btn-secondary" /></p>
        </form>
    </div>
 
    </body>
    </html>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>
</html>