package com.example.edukids;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.SwitchCompat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import  androidx.appcompat.widget.SwitchCompat;
import android.widget.TextView;

public class SettingActivity extends AppCompatActivity {

    public static boolean mIsNightMode = false;
    int mColor,mAA;

    int warna,warna1;

    // Key for current color
    private final String COLOR_KEY = "color";

    // Shared preferences object
    private SharedPreferences mPreferences;
    // Name of shared preferences file

    SharedPreferences.Editor preferencesEditor;
    SwitchCompat swtema,swsize;
    TextView txtDark, txtAbout;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

       swtema = findViewById(R.id.switch_dark_mode);
       linearLayout = findViewById(R.id.parentLy);
       txtDark = findViewById(R.id.tulisanDark);
       txtAbout = findViewById(R.id.about);
       mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
       mColor = mPreferences.getInt("background", 0);
       mAA = mColor;

        if (mAA != 0){
            linearLayout.setBackgroundColor(getResources().getColor(R.color.hitam));
            txtDark.setTextColor(getResources().getColor(R.color.white));
            txtAbout.setTextColor(getResources().getColor(R.color.white));
            swtema.setChecked(true);
        } else {
            swtema.setChecked(false);
            txtDark.setTextColor(getResources().getColor(R.color.hitam));
            txtAbout.setTextColor(getResources().getColor(R.color.hitam));
            linearLayout.setBackgroundColor(getResources().getColor(R.color.white));
        }

//        SharedPreferences pref = getApplicationContext().getSharedPreferences("pref", 0);
//
//        preferencesEditor = pref.edit();
//
//        warna = pref.getInt("background",R.color.white);
//        warna1 = pref.getInt("background",R.color.white);

        swtema.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("pref", 0);

                    preferencesEditor = mPreferences.edit();

//                    warna = pref.getInt("background",R.color.hitam);
                    preferencesEditor.putInt("background", 1);
                    preferencesEditor.commit();
                } else {
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("pref", 0);

                    preferencesEditor = mPreferences.edit();

                    warna = pref.getInt("background",R.color.white);
                    preferencesEditor.putInt("background",0);
                    preferencesEditor.commit();

                }
            }
        });



    }

    @Override
    protected void onResume() {
        super.onResume();
        mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mColor = mPreferences.getInt("background", 0);
        mAA = mColor;

        if (mAA != 0){
            linearLayout.setBackgroundColor(getResources().getColor(R.color.hitam));
            txtDark.setTextColor(getResources().getColor(R.color.white));
            txtAbout.setTextColor(getResources().getColor(R.color.white));

        } else {
            txtDark.setTextColor(getResources().getColor(R.color.hitam));
            txtAbout.setTextColor(getResources().getColor(R.color.hitam));
            linearLayout.setBackgroundColor(getResources().getColor(R.color.white));


        }
    }

    public void save(View view) {

        if (swtema.isChecked()==true){


//
//            txttema.setTextColor(getResources().getColor(R.color.default_background));
//            txtsize.setTextColor(getResources().getColor(R.color.default_background));
//            jdul.setTextColor(getResources().getColor(R.color.default_background));
//
//            linearLayout.setBackgroundColor(getResources().getColor(R.color.black));

//            Intent pindah = new Intent(SettingActivity.this,MainActivity.class);
//            startActivity(pindah);
        } else {


//            txttema.setTextColor(getResources().getColor(R.color.black));
//            txtsize.setTextColor(getResources().getColor(R.color.black));
//            jdul.setTextColor(getResources().getColor(R.color.black));
//
//            linearLayout.setBackgroundColor(getResources().getColor(R.color.default_background));


//            Intent pindah1 = new Intent(SettingActivity.this,MainActivity.class);
//            startActivity(pindah1);
        }

    }

    public void btnAbout(View view) {
        Intent pindah = new Intent(SettingActivity.this, AboutActivity.class);
        startActivity(pindah);
    }
}
