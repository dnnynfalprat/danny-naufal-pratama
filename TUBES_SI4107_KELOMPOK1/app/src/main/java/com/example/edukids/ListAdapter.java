package com.example.edukids;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    private ArrayList<Article> articles = new ArrayList<>();
    ListAdapter(ArrayList<Article> articles) {
        this.articles.clear();
        this.articles = articles;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_article, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListAdapter.ViewHolder holder, int position) {
        holder.bind(articles.get(position));
    }

    @Override
    public int getItemCount() {
        return articles.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvListJudul, tvListCerita, tvListTanggal;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvListJudul = itemView.findViewById(R.id.tv_list_judul);
            tvListCerita = itemView.findViewById(R.id.tv_list_cerita);
            tvListTanggal = itemView.findViewById(R.id.tv_list_tanggal);
            itemView.setOnClickListener(this);
        }

        public void bind(Article article) {
            tvListJudul.setText(article.getJudul());
            tvListCerita.setText(article.getCerita());
            tvListTanggal.setText(article.getTanggal());
        }

        @Override
        public void onClick(View view) {
            Article article = articles.get(getAdapterPosition());

            Intent intent = new Intent(view.getContext(), DetailActivity2.class);
            intent.putExtra("Id", article.getId());
            view.getContext().startActivity(intent);
        }
    }
}