package com.example.edukids;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class PerhitunganActivity extends AppCompatActivity {

    private SharedPreferences pref;
    private final String KEY_NAME = "key_name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perhitungan);

        pref = getSharedPreferences("mypreferences", MODE_PRIVATE);

        showSavedName();

        final EditText etName = (EditText) findViewById(R.id.et_name);
        Button ToMengenal = findViewById(R.id.mengenal);
        Button ToPerhitungan = findViewById(R.id.perhitungan);

        final Button bSave = (Button) findViewById(R.id.bt_save);
        bSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                bSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String name = etName.getText().toString();
                        if(name.equals("")){
                            Toast.makeText(PerhitunganActivity.this,
                                    "Nama tidak boleh kosong",
                                    Toast.LENGTH_SHORT).show();
                        }else{
                            saveName(name);
                            showSavedName();

                        }
                    }
                });
            }
        });


        ToMengenal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(PerhitunganActivity.this, KuisActivity.class );
                startActivity(pindah);
            }
        });

        ToPerhitungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(PerhitunganActivity.this,HalamanBermain.class );
                startActivity(pindah);
            }
        });

    }

    private void showSavedName(){
        String savedName = getSavedName();

        TextView tvSavedName = (TextView) findViewById(R.id.tv_saved_name);
        tvSavedName.setText(savedName);
    }

    private String getSavedName(){
        return pref.getString(KEY_NAME, "-");
    }

    private void saveName(String name){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(KEY_NAME, name);
        editor.commit();


    }
}



