package com.example.edukids;

import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import android.os.Bundle;

public class CreateActivity extends AppCompatActivity {

    DatabaseHelper myDb;
    private EditText etJudul, etCerita, etTanggal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        setTitle("Buat Diary");

        myDb = new DatabaseHelper(this);
        etJudul = findViewById(R.id.edt_input_judul);
        etCerita = findViewById(R.id.edt_input_cerita);
        etTanggal = findViewById(R.id.edt_input_tanggal);
        ImageView toBack = findViewById(R.id.to_back);

        toBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(CreateActivity.this, Main3Activity.class );
                startActivity(pindah);
            }
        });
    }


    public void clickPost(View view) {
        String judul = etJudul.getText().toString();
        String cerita = etCerita.getText().toString();
        String tanggal = etTanggal.getText().toString();

        if (judul.equals("") || cerita.equals("") || tanggal.equals("")) {
            Toast.makeText(this, "Mohon isi data secara lengkap", Toast.LENGTH_SHORT).show();
        } else {
            boolean insert = myDb.insertData(judul, cerita, tanggal);
            if (insert){
                etJudul.setText("");
                etCerita.setText("");
                etTanggal.setText("");
                Toast.makeText(this, "Artikel berhasil di tambahkan", Toast.LENGTH_SHORT).show();
            } else{
                Toast.makeText(this, "Artikel gagal di tambahkan", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

