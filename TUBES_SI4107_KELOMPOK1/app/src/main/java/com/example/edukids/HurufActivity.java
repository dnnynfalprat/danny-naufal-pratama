package com.example.edukids;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class HurufActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_huruf);

        final TextView EtHurufa =  findViewById(R.id.hurufa);
        final TextView EtHurufb =  findViewById(R.id.hurufb);
        final TextView EtHurufc =  findViewById(R.id.hurufc);
        final TextView EtHurufd =  findViewById(R.id.hurufd);
        final TextView EtHurufe =  findViewById(R.id.hurufe);
        final TextView EtHuruff =  findViewById(R.id.huruff);
        final TextView EtHurufg =  findViewById(R.id.hurufg);
        final TextView EtHurufh =  findViewById(R.id.hurufh);
        final TextView EtHurufi =  findViewById(R.id.hurufi);
        final TextView EtHurufj =  findViewById(R.id.hurufj);
        final TextView EtHurufk =  findViewById(R.id.hurufk);
        final TextView EtHurufl =  findViewById(R.id.hurufl);
        final TextView EtHurufm =  findViewById(R.id.hurufm);
        final TextView EtHurufn =  findViewById(R.id.hurufn);
        final TextView EtHurufo =  findViewById(R.id.hurufo);
        final TextView EtHurufp =  findViewById(R.id.hurufp);
        final TextView EtHurufq =  findViewById(R.id.hurufq);
        final TextView EtHurufr =  findViewById(R.id.hurufr);
        final TextView EtHurufs =  findViewById(R.id.hurufs);
        final TextView EtHuruft =  findViewById(R.id.huruft);
        final TextView EtHurufu =  findViewById(R.id.hurufu);
        final TextView EtHurufv =  findViewById(R.id.hurufv);
        final TextView EtHurufw =  findViewById(R.id.hurufw);
        final TextView EtHurufx=   findViewById(R.id.hurufx);
        final TextView EtHurufy =  findViewById(R.id.hurufy);
        final TextView EtHurufz =  findViewById(R.id.hurufz);
        ImageView toBack = findViewById(R.id.to_back);




        EtHurufa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("alphabet","A");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "A (INDONESIAN)");
                pindah.putExtra("ejaan","E-I (ENGLISH)");
                pindah.putExtra("info","ayam\n" +
                        "angsa\n" +
                        "anggur\n" +
                        "apel\n" +
                        "anjing\n" +
                        "anting\n" +
                        "anggrek\n" +
                        "api\n" +
                        "air\n" +
                        "angin\n" +
                        "akar\n" +
                        "amplop\n" +
                        "angklung\n" +
                        "aquarium\n" +
                        "arang\n" +
                        "asbak\n" +
                        "awan\n" +
                        "alpukat\n" +
                        "alamat\n" +
                        "alat");
                startActivity(pindah);
            }
        });

        EtHurufb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","B");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "B (INDONESIAN)");
                pindah.putExtra("ejaan","B-I (ENGLISH)");
                pindah.putExtra("info","babi\n" +
                        "bayi\n" +
                        "bebek\n" +
                        "bunga\n" +
                        "bayam\n" +
                        "buaya\n" +
                        "boneka\n" +
                        "beruang\n" +
                        "benteng\n" +
                        "bangau\n" +
                        "badak\n" +
                        "badai\n" +
                        "badut\n" +
                        "botol\n" +
                        "bulu\n" +
                        "bakso\n" +
                        "batu\n" +
                        "bandara\n" +
                        "bolu\n" +
                        "bantal");
                startActivity(pindah);
            }
        });

        EtHurufc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","C");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "C (INDONESIAN)");
                pindah.putExtra("ejaan","S-I (ENGLISH)");
                pindah.putExtra("info","cicak\n" +
                        "cacing\n" +
                        "cemara\n" +
                        "cinta\n" +
                        "cahaya\n" +
                        "cangkir\n" +
                        "cap\n" +
                        "cebong\n" +
                        "catur\n" +
                        "curam\n" +
                        "cawan\n" +
                        "capung\n" +
                        "cempaka\n" +
                        "cemburu\n" +
                        "cendrawasih\n" +
                        "cendol\n" +
                        "cengkeh\n" +
                        "cincin\n" +
                        "cilok\n" +
                        "cowok\n");
                startActivity(pindah);
            }
        });

        EtHurufd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","D");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "D (INDONESIAN)");
                pindah.putExtra("ejaan","D-I (ENGLISH)");
                pindah.putExtra("info","dadu\n" +
                        "dapur\n" +
                        "domba\n" +
                        "daging\n" +
                        "duri\n" +
                        "durian\n" +
                        "delima\n" +
                        "dansa\n" +
                        "darah\n" +
                        "dasi\n" +
                        "daun\n" +
                        "demak\n" +
                        "dendam\n" +
                        "dengki\n" +
                        "demam\n" +
                        "dermaga\n" +
                        "dewa\n" +
                        "dewi\n" +
                        "diam\n" +
                        "dewasa");
                startActivity(pindah);
            }
        });

        EtHurufe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","E");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "E (INDONESIAN)");
                pindah.putExtra("ejaan","I (ENGLISH)");
                pindah.putExtra("info","elang\n" +
                        "ekor\n" +
                        "egois\n" +
                        "editor\n" +
                        "ekonomi\n" +
                        "ekologi\n" +
                        "ekosistem\n" +
                        "eksperimen\n" +
                        "ekspor\n" +
                        "eksternal\n" +
                        "evolusi\n" +
                        "elemen\n" +
                        "ember\n" +
                        "emosi\n" +
                        "empati\n" +
                        "energi\n" +
                        "enzim\n" +
                        "eksekutor\n" +
                        "es\n" +
                        "emas\n");
                startActivity(pindah);
            }
        });

        EtHuruff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","F");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "F (INDONESIAN)");
                pindah.putExtra("ejaan","E-F (ENGLISH)");
                pindah.putExtra("info","film\n" +
                        "flamboyan\n" +
                        "fosil\n" +
                        "foto\n" +
                        "fase\n" +
                        "februari\n" +
                        "fiksi\n" +
                        "flu\n" +
                        "fokus\n" +
                        "fisik\n" +
                        "filosofi\n" +
                        "firasat\n" +
                        "fisika\n" +
                        "fitnah\n" +
                        "flamingo\n" +
                        "formulir\n" +
                        "fakta\n" +
                        "fase\n" +
                        "fauna\n" +
                        "flora");
                startActivity(pindah);
            }
        });

        EtHurufg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","G");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "G (INDONESIAN)");
                pindah.putExtra("ejaan","J-I (ENGLISH)");
                pindah.putExtra("info","gajah\n" +
                        "gendang\n" +
                        "gereja\n" +
                        "gunung\n" +
                        "gembok\n" +
                        "gayung\n" +
                        "gergaji\n" +
                        "gula\n" +
                        "genteng\n" +
                        "gunting\n" +
                        "gabus\n" +
                        "gandum\n" +
                        "garam\n" +
                        "garuda\n" +
                        "garasi\n" +
                        "golok\n" +
                        "garpu\n" +
                        "gas\n" +
                        "gedung\n" +
                        "gaun");
                startActivity(pindah);
            }
        });

        EtHurufh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","H");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "H (INDONESIAN)");
                pindah.putExtra("ejaan","E-I-J (ENGLISH)");
                pindah.putExtra("info","hari\n" +
                        "habitat\n" +
                        "hakim\n" +
                        "handuk\n" +
                        "hantu\n" +
                        "hangus\n" +
                        "harimau\n" +
                        "hiu\n" +
                        "helm\n" +
                        "hati\n" +
                        "helikopter\n" +
                        "haus\n" +
                        "hijau\n" +
                        "hijab\n" +
                        "hidung\n" +
                        "hitam\n" +
                        "hobi\n" +
                        "hoki\n" +
                        "hujan\n" +
                        "hutan");
                startActivity(pindah);
            }
        });

        EtHurufi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","I");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "I (INDONESIAN)");
                pindah.putExtra("ejaan","A-I (ENGLISH)");
                pindah.putExtra("info","ikan\n" +
                        "ide\n" +
                        "idola\n" +
                        "idaman\n" +
                        "ingatan\n" +
                        "ikat\n" +
                        "ilmu\n" +
                        "istirahat\n" +
                        "imam\n" +
                        "imlek\n" +
                        "ilegal\n" +
                        "iklim\n" +
                        "imun\n" +
                        "indigo\n" +
                        "informasi\n" +
                        "infeksi\n" +
                        "inkubator\n" +
                        "insiden\n" +
                        "internal\n" +
                        "itik");
                startActivity(pindah);
            }
        });

        EtHurufj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","J");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "J (INDONESIAN)");
                pindah.putExtra("ejaan","J-E-Y (ENGLISH)");
                pindah.putExtra("info","jari\n" +
                        "jaket\n" +
                        "jemuran\n" +
                        "jerami\n" +
                        "jambu\n" +
                        "jerapah\n" +
                        "jaguar\n" +
                        "jam \n" +
                        "jagung\n" +
                        "jamu\n" +
                        "januari\n" +
                        "jamur\n" +
                        "jangkar\n" +
                        "jendela\n" +
                        "jawa\n" +
                        "jengkol\n" +
                        "jas\n" +
                        "jeruk\n" +
                        "jarum\n" +
                        "jangkrik\n");
                startActivity(pindah);
            }
        });

        EtHurufk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","K");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "K (INDONESIAN)");
                pindah.putExtra("ejaan","K-E-Y (ENGLISH)");
                pindah.putExtra("info","kue\n" +
                        "kumis\n" +
                        "kecoak\n" +
                        "kambing\n" +
                        "kerbau\n" +
                        "kucing\n" +
                        "kelinci\n" +
                        "kacang\n" +
                        "kumbang\n" +
                        "kaktus\n" +
                        "kereta\n" +
                        "kemeja\n" +
                        "kasur\n" +
                        "kertas\n" +
                        "kursi\n" +
                        "kapsul\n" +
                        "koran\n" +
                        "kedondong\n" +
                        "kalimantan\n" +
                        "karung\n");
                startActivity(pindah);
            }
        });

        EtHurufl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","L");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "L (INDONESIAN)");
                pindah.putExtra("ejaan","E-L (ENGLISH)");
                pindah.putExtra("info","lintah\n" +
                        "lumut\n" +
                        "lemari\n" +
                        "lukisan\n" +
                        "lantai\n" +
                        "laci\n" +
                        "ladang\n" +
                        "lalat\n" +
                        "lagu\n" +
                        "lambang\n" +
                        "lambung\n" +
                        "landak\n" +
                        "laptop\n" +
                        "laut\n" +
                        "lebah\n" +
                        "leci\n" +
                        "lele\n" +
                        "lemper\n" +
                        "lemon\n" +
                        "lentera\n");
                startActivity(pindah);
            }
        });

        EtHurufm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","M");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "M (INDONESIAN)");
                pindah.putExtra("ejaan","E-M (ENGLISH)");
                pindah.putExtra("info","meja\n" +
                        "mangga\n" +
                        "manggis\n" +
                        "merah\n" +
                        "manusia\n" +
                        "macan\n" +
                        "merak\n" +
                        "maaf\n" +
                        "macet\n" +
                        "madu\n" +
                        "makan\n" +
                        "minum\n" +
                        "masuk\n" +
                        "mandi\n" +
                        "malu\n" +
                        "malas\n" +
                        "monyet\n" +
                        "mangkok\n" +
                        "mantel\n" +
                        "mantan");
                startActivity(pindah);
            }
        });

        EtHurufn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","N");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "N (INDONESIAN)");
                pindah.putExtra("ejaan","E-N (ENGLISH)");
                pindah.putExtra("info","nasi\n" +
                        "nuri\n" +
                        "naga\n" +
                        "nafsu\n" +
                        "nabati\n" +
                        "nasabah\n" +
                        "netral\n" +
                        "net \n" +
                        "ngengat\n" +
                        "nirwana\n" +
                        "nitrogen\n" +
                        "nol\n" +
                        "nota\n" +
                        "novel\n" +
                        "nutrisi\n" +
                        "nominal\n" +
                        "november\n" +
                        "nyamuk\n" +
                        "nyala\n" +
                        "nyaman\n");
                startActivity(pindah);
            }
        });

        EtHurufo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","O");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "O (INDONESIAN)");
                pindah.putExtra("ejaan","O-U (ENGLISH)");
                pindah.putExtra("info","obat\n" +
                        "oktober\n" +
                        "oksigen\n" +
                        "ojek\n" +
                        "obor\n" +
                        "objek\n" +
                        "odol\n" +
                        "oli\n" +
                        "oncom\n" +
                        "ongkos\n" +
                        "obeng\n" +
                        "olahraga\n" +
                        "operasi\n" +
                        "oreo\n" +
                        "opname\n" +
                        "opera \n" +
                        "otak\n" +
                        "otomotif\n" +
                        "ozon\n" +
                        "oposisi");
                startActivity(pindah);
            }
        });

        EtHurufp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","P");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "P (INDONESIAN)");
                pindah.putExtra("ejaan","P-I (ENGLISH)");
                pindah.putExtra("info","pantai\n" +
                        "pintu\n" +
                        "pagar\n" +
                        "piring\n" +
                        "parang\n" +
                        "padi\n" +
                        "panda\n" +
                        "pacar\n" +
                        "palu\n" +
                        "paku\n" +
                        "pelukis\n" +
                        "panah\n" +
                        "perak\n" +
                        "perunggu\n" +
                        "panci\n" +
                        "parasit\n" +
                        "parfum\n" +
                        "papua\n" +
                        "parit\n" +
                        "parasut\n");
                startActivity(pindah);
            }
        });

        EtHurufq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","Q");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "Q (INDONESIAN)");
                pindah.putExtra("ejaan","K-Y-U (ENGLISH)");
                pindah.putExtra("info","queen = ratu\n" +
                        "quality = kualitas\n" +
                        "quarantine = karantina\n" +
                        "quantitative = kuantitatif\n" +
                        "quaint = aneh\n" +
                        "quadratic = kuadrat\n" +
                        "quiet = diam\n" +
                        "question = pertanyaan\n" +
                        "quirky = unik\n" +
                        "queue = antri");
                startActivity(pindah);
            }
        });

        EtHurufr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","R");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "R (INDONESIAN)");
                pindah.putExtra("ejaan","A-R (ENGLISH)");
                pindah.putExtra("info","remaja\n" +
                        "rambutan\n" +
                        "rumah\n" +
                        "ranjau\n" +
                        "rimba\n" +
                        "remote\n" +
                        "rokok\n" +
                        "rantai\n" +
                        "roda\n" +
                        "racun\n" +
                        "rubah\n" +
                        "radio\n" +
                        "raket\n" +
                        "raksasa\n" +
                        "rajawali\n" +
                        "raja \n" +
                        "ranting\n" +
                        "rawon\n" +
                        "rayap\n" +
                        "reptil");
                startActivity(pindah);
            }
        });

        EtHurufs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","S");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "S (INDONESIAN)");
                pindah.putExtra("ejaan","E-S (ENGLISH)");
                pindah.putExtra("info","sikat\n" +
                        "sapu\n" +
                        "sendok\n" +
                        "sepeda\n" +
                        "sepatu\n" +
                        "sandal\n" +
                        "sumatera\n" +
                        "suling\n" +
                        "sisir\n" +
                        "sabun\n" +
                        "susu\n" +
                        "sirsak\n" +
                        "semangka\n" +
                        "singa\n" +
                        "sakura\n" +
                        "sakelar\n" +
                        "salmon\n" +
                        "salak\n" +
                        "sawah\n" +
                        "sulawesi\n");
                startActivity(pindah);
            }
        });

        EtHuruft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","T");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "T (INDONESIAN)");
                pindah.putExtra("ejaan","T-I (ENGLISH)");
                pindah.putExtra("info","timur\n" +
                        "tenda\n" +
                        "tirai\n" +
                        "tikar\n" +
                        "tikus\n" +
                        "televisi\n" +
                        "telepon\n" +
                        "tambang\n" +
                        "tajam\n" +
                        "talenta\n" +
                        "tanduk\n" +
                        "tempe\n" +
                        "tahu\n" +
                        "tapir\n" +
                        "tupai\n" +
                        "tato\n" +
                        "taring\n" +
                        "tari \n" +
                        "tembok\n" +
                        "tebu");
                startActivity(pindah);
            }
        });

        EtHurufu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","U");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "U (INDONESIAN)");
                pindah.putExtra("ejaan","Y-U (ENGLISH)");
                pindah.putExtra("info","unta\n" +
                        "uang\n" +
                        "uban\n" +
                        "ukulele\n" +
                        "ular\n" +
                        "udara\n" +
                        "udang\n" +
                        "ubi\n" +
                        "ubin\n" +
                        "unggas\n" +
                        "ungu\n" +
                        "universitas\n" +
                        "utara\n" +
                        "usia\n" +
                        "universal\n" +
                        "uap\n" +
                        "ultrasonik\n" +
                        "undangan\n" +
                        "unik\n" +
                        "upacara");
                startActivity(pindah);
            }
        });

        EtHurufv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","V");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "V (INDONESIAN)");
                pindah.putExtra("ejaan","V-I (ENGLISH)");
                pindah.putExtra("info","ventilasi\n" +
                        "vas\n" +
                        "vaksin\n" +
                        "vakum\n" +
                        "verbal\n" +
                        "valid\n" +
                        "vatikan\n" +
                        "vertikal\n" +
                        "video\n" +
                        "vespa\n" +
                        "voli\n" +
                        "visual\n" +
                        "villa\n" +
                        "vokalis\n" +
                        "visum\n" +
                        "virus\n" +
                        "versus\n" +
                        "violet\n" +
                        "vodka\n" +
                        "visi");


                startActivity(pindah);
            }
        });

        EtHurufw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","W");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "W (INDONESIAN)");
                pindah.putExtra("ejaan","D-A-B-E-L-Y-U (ENGLISH)");
                pindah.putExtra("info","waktu\n" +
                        "wabah\n" +
                        "waduk\n" +
                        "walet\n" +
                        "wajan\n" +
                        "wakil\n" +
                        "wanita\n" +
                        "wangsit\n" +
                        "wali\n" +
                        "wasit\n" +
                        "wayang\n" +
                        "wawancara\n" +
                        "warung\n" +
                        "wastafel\n" +
                        "watak\n" +
                        "wig\n" +
                        "wisma\n" +
                        "wortel\n" +
                        "wilayah\n" +
                        "wisuda");
                startActivity(pindah);
            }
        });

        EtHurufx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","X");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "X (INDONESIAN)");
                pindah.putExtra("ejaan","E-K-S (ENGLISH)");
                pindah.putExtra("info","xilofon\n" +
                        "xenia\n" +
                        "xerofit\n" +
                        "xilena\n" +
                        "xilosa\n" +
                        "xilograf\n" +
                        "xenofili\n" +
                        "x-ray\n" +
                        "xenolit\n" +
                        "xenofobia\n" +
                        "xerosis\n" +
                        "xenon\n" +
                        "xifoid\n" +
                        "xantofil\n" +
                        "xilem\n" +
                        "xiloid \n" +
                        "xilol\n" +
                        "xantat\n" +
                        "xantena\n" +
                        "xeroftalmia");
                startActivity(pindah);
            }
        });

        EtHurufy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","Y");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "Y (INDONESIAN)");
                pindah.putExtra("ejaan","W-A-Y (ENGLISH)");
                pindah.putExtra("info","yoyo\n" +
                        "yuyu\n" +
                        "yudisium\n" +
                        "yoga\n" +
                        "yudikatif\n" +
                        "yogyakarta\n" +
                        "yayasan\n" +
                        "yoghurt\n" +
                        "yahudi\n" +
                        "yunani\n" +
                        "yen\n" +
                        "yahwe\n" +
                        "yamtuan\n" +
                        "yuridis\n" +
                        "yupiter\n" +
                        "yubileum\n" +
                        "yatim\n" +
                        "yakin\n" +
                        "yaitu\n" +
                        "yakni");
                startActivity(pindah);
            }
        });

        EtHurufz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, DetailActivity.class );
                pindah.putExtra("alphabet","Z");
                pindah.putExtra("untuk","huruf");
                pindah.putExtra("status","text");
                pindah.putExtra("nama", "Z (INDONESIAN)");
                pindah.putExtra("ejaan","Z-I (ENGLISH)");
                pindah.putExtra("info","zebra\n" +
                        "zat\n" +
                        "zaman\n" +
                        "zonasi\n" +
                        "zodiak\n" +
                        "zakat\n" +
                        "ziarah\n" +
                        "zamzam\n" +
                        "zamrud\n" +
                        "zero\n" +
                        "zenit\n" +
                        "zoologi\n" +
                        "zimolisis\n" +
                        "zantara\n" +
                        "zaitun\n" +
                        "zion\n" +
                        "zoofobia\n" +
                        "zimotik\n" +
                        "zeoponik\n" +
                        "zabur");
                startActivity(pindah);
            }
        });


        toBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(HurufActivity.this, Dashboard.class );
                startActivity(pindah);
            }
        });


    }
}
