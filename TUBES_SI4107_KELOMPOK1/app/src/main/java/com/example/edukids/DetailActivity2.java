package com.example.edukids;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.os.Bundle;

public class DetailActivity2 extends AppCompatActivity {

    DatabaseHelper myDb;
    private TextView tvDetailJudul, tvDetailCerita, tvDetailTanggal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail2);

        setTitle("Detail Diary");

        myDb = new DatabaseHelper(this);
        tvDetailJudul = findViewById(R.id.tv_detail_judul);
        tvDetailCerita = findViewById(R.id.tv_detail_cerita);
        tvDetailTanggal = findViewById(R.id.tv_detail_tanggal);
        ImageView toBack = findViewById(R.id.to_back);

        toBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(DetailActivity2.this, ListActivity.class );
                startActivity(pindah);
            }
        });

        int id = getIntent().getIntExtra("Id", 0);
        Cursor cursor = myDb.getDataById(id);

        cursor.moveToNext();
        tvDetailJudul.setText(cursor.getString(1));
        tvDetailCerita.setText(cursor.getString(2));
        tvDetailTanggal.setText(cursor.getString(3));
    }
}
