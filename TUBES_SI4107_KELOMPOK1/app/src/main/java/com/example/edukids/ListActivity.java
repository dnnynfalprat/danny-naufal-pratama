package com.example.edukids;

import android.content.Intent;
import android.os.Bundle;
import android.database.Cursor;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    private RecyclerView rvListDiary;
    DatabaseHelper myDb;
    private ArrayList<Article> articles = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        setTitle("Diary List");
        rvListDiary = findViewById(R.id.rv_list_diary);
        ImageView toBack = findViewById(R.id.to_back);
        myDb = new DatabaseHelper(this);

        toBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(ListActivity.this, Main3Activity.class );
                startActivity(pindah);
            }
        });

        getDataDiary();

        rvListDiary.setLayoutManager(new LinearLayoutManager(this));
        rvListDiary.setAdapter(new ListAdapter(articles));
    }

    private void getDataDiary() {
        Cursor cursor = myDb.getAllData();

        while (cursor.moveToNext()){
            Article article = new Article();
            article.setId(cursor.getInt(0));
            article.setJudul(cursor.getString(1));
            article.setCerita(cursor.getString(2));
            article.setTanggal(cursor.getString(3));

            articles.add(article);
        }
    }
}
