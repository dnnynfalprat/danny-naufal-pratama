package com.example.edukids.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.edukids.Dashboard;
import com.example.edukids.FeedbackActivity;
import com.example.edukids.Main2Activity;
import com.example.edukids.Main3Activity;
import com.example.edukids.MenuAwalActivity;
import com.example.edukids.PerhitunganActivity;
import com.example.edukids.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    public HomeFragment() {
        // Required empty public constructor
    }

    public static final String TAG = "homeFragment";
    private ImageView imgBelajar, imgDiari, imgConsole, imgFeedback;
    private LinearLayout linearLayout;
    private TextView textView;
    int mColor,mAA;

    int warna,warna1;

    // Key for current color
    private final String COLOR_KEY = "color";

    // Shared preferences object
    private SharedPreferences mPreferences;
    // Name of shared preferences file

    SharedPreferences.Editor preferencesEditor;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        imgBelajar = view.findViewById(R.id.belajar);
        imgDiari = view.findViewById(R.id.diary);
        imgConsole = view.findViewById(R.id.game);
        imgFeedback = view.findViewById(R.id.feedback);
        linearLayout = view.findViewById(R.id.parentLy);
        mPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext());
        mColor = mPreferences.getInt("background", 0);
        mAA = mColor;

        if (mAA != 0){
            linearLayout.setBackgroundColor(getResources().getColor(R.color.hitam));
        } else {
            linearLayout.setBackgroundColor(getResources().getColor(R.color.white));
        }

        imgBelajar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(requireContext(), Dashboard.class );
                startActivity(pindah);
            }
        });

        imgDiari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(requireContext(), Main3Activity.class );
                startActivity(pindah);
            }
        });

        imgConsole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(requireContext(), PerhitunganActivity.class );
                startActivity(pindah);
            }
        });

        imgFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(requireContext(), FeedbackActivity.class );
                startActivity(pindah);
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext());
        mColor = mPreferences.getInt("background", 0);
        mAA = mColor;

        if (mAA != 0){
            linearLayout.setBackgroundColor(getResources().getColor(R.color.hitam));
        } else {
            linearLayout.setBackgroundColor(getResources().getColor(R.color.white));
        }

    }
}
