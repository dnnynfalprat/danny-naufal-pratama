package com.example.edukids;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class TerimakasihActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terimakasih);

        TextView TvTerimakasih = findViewById(R.id.terimakasih);

    }
    public void Click1(View view) {
        EditText text1 = (EditText) findViewById(R.id.edit1);
        EditText text2 = (EditText) findViewById(R.id.edit2);
        // Write a message to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("Saran").child(text1.getText().toString());
        myRef.child("saran").setValue(text2.getText().toString());

        Intent pindah = new Intent(TerimakasihActivity.this, TrimsActivity.class);

        Toast.makeText(getApplicationContext(), "SARAN TERKIRIM" , Toast.LENGTH_SHORT).show();
        startActivity(pindah);

    }
}

