package com.example.edukids;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class Main3Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        ImageView toProfile = findViewById(R.id.to_profile);
        ImageView toBack = findViewById(R.id.to_back);

        toProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(Main3Activity.this, Main2Activity.class );
                startActivity(pindah);
            }
        });

        toBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(Main3Activity.this, MenuAwalActivity.class );
                startActivity(pindah);
            }
        });

        setTitle("Home Article");
    }

    public void clickList(View view) {
        startActivity(new Intent(this, ListActivity.class));
    }

    public void clickCreate(View view) {
        startActivity(new Intent(this, CreateActivity.class));
    }


}
