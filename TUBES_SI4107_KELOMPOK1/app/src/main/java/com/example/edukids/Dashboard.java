package com.example.edukids;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.security.interfaces.DSAKey;

public class Dashboard extends AppCompatActivity {

    private ImageView imgAngka, imgHuruf, imgBinatang, imgBuah, imgSayur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        imgAngka = findViewById(R.id.angkaImg);
        imgHuruf = findViewById(R.id.hurufImg);
        imgBinatang = findViewById(R.id.binatangImg);
        imgBuah = findViewById(R.id.buahImg);
        imgSayur = findViewById(R.id.sayurImg);
        ImageView toBack = findViewById(R.id.to_back);

        imgAngka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(Dashboard.this, AngkaActivity.class );
                startActivity(pindah);
            }
        });

        imgHuruf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(Dashboard.this, HurufActivity.class );
                startActivity(pindah);
            }
        });

        imgBinatang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(Dashboard.this, binatangActivity.class );
                startActivity(pindah);
            }
        });

        imgBuah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(Dashboard.this, buahActivity.class );
                startActivity(pindah);
            }
        });

        imgSayur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(Dashboard.this, sayurActivity.class );
                startActivity(pindah);
            }
        });


        toBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(Dashboard.this, MenuAwalActivity.class );
                startActivity(pindah);
            }
        });

    }

}
