package com.example.edukids;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        ImageView tvGambar = findViewById(R.id.tv_gambar);
        TextView tvText = findViewById(R.id.tv_text);
        TextView tvNama =  findViewById(R.id.tv_nama);
        TextView tvEjaan =  findViewById(R.id.tv_ejaan);
        TextView tvInfo = findViewById(R.id.tv_info);
        ImageView toBack = findViewById(R.id.to_back);



        tvNama.setText(getIntent().getStringExtra("nama"));
        tvEjaan.setText(getIntent().getStringExtra("ejaan"));
        tvInfo.setText(getIntent().getStringExtra("info"));


        if (getIntent().getStringExtra("status").equals("image")){
            tvGambar.setVisibility(View.VISIBLE);
            tvGambar.setImageResource(getIntent().getIntExtra("gambar",0));
        } else {
            tvText.setVisibility(View.VISIBLE);

            if (getIntent().getStringExtra("untuk").equals("angka")){
                tvText.setText(String.valueOf(getIntent().getIntExtra("alpha",0)));

            } else {
                tvText.setText(String.valueOf(getIntent().getStringExtra("alphabet")));

            }

        }


        toBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(DetailActivity.this, Dashboard.class );
                startActivity(pindah);
            }
        });
    }
}
