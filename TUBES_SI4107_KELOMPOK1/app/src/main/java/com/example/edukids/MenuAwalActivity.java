package com.example.edukids;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.edukids.fragment.HomeFragment;
import com.example.edukids.fragment.ProfileFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MenuAwalActivity extends AppCompatActivity {

    FirebaseAuth mAuth;
    private BottomNavigationView mBottomNav;
    private HomeFragment homeFragment;
    private ProfileFragment profileFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_awal);

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        if (user == null) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        } else {
        mBottomNav = findViewById(R.id.bottomnav_menu);
            mBottomNav.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
            homeFragment = new HomeFragment();
            profileFragment = new ProfileFragment();

            LoadHomeFragment();
        }

    }

    private void LoadHomeFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.main_content, homeFragment, "Home Fragment");
        fragmentTransaction.commitAllowingStateLoss();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener
            mOnNavigationItemSelectedListener = menuItem -> {
        Fragment fragment = null;
        switch (menuItem.getItemId()) {
            case R.id.navigation_home:
                onchangeSegmentFragment(homeFragment, HomeFragment.TAG);
                return true;
            case R.id.navigation_profile:
                onchangeSegmentFragment(profileFragment, ProfileFragment.TAG);
                return true;
        }
        return false;
    };

    private void onchangeSegmentFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager2 = getSupportFragmentManager();
        fragmentManager2.beginTransaction().
                disallowAddToBackStack().
                replace(R.id.main_content, fragment).
                commit();
    }

}

