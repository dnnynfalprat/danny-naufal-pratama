package com.example.edukids;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class buahActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buah);

        final ImageView ImgAlpukat =  findViewById(R.id.alpukatImg);
        final ImageView ImgAnggur =  findViewById(R.id.anggurImg);
        final ImageView ImgApel =  findViewById(R.id.apelImg);
        final ImageView ImgBelimbing =  findViewById(R.id.belimbingImg);
        final ImageView ImgCeri =  findViewById(R.id.ceriImg);
        final ImageView ImgKelapa =  findViewById(R.id.kelapaImg);
        final ImageView ImgMannga =  findViewById(R.id.manggaImg);
        final ImageView ImgMelon =  findViewById(R.id.melonImg);
        final ImageView ImgPisang =  findViewById(R.id.pisangImg);
        final ImageView ImgStrawberry =  findViewById(R.id.strawberryImg);
        ImageView toBack = findViewById(R.id.to_back);

        ImgAlpukat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(buahActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.alpukat);
                pindah.putExtra("nama", "A L P U K A T");
                pindah.putExtra("ejaan","A - L - P - U - K - A - T");
                pindah.putExtra("info","Buah Alpukat atau yang di dalam bahasa Inggrisnya dikenal dengan Avocado\n" +
                        "mempunyai cukup banyak manfaat. Di antaranya adalah ia mampu menangkal adanya\n" +
                        "radikal bebas atau berbagai jenis racun di dalam tubuh manusia.\n" +
                        "Buah Alpukat sendiri kerap dipakai untuk membuat jus.\n" +
                        "Campuran untuk es campur dan berbagai hidangan untuk jenis minuman segar yang lain.\n" +
                        "Buah ini juga mempunyai biji cukup besar");
                startActivity(pindah);
            }
        });


        ImgAnggur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(buahActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.anggur);
                pindah.putExtra("nama", "A N G G U R");
                pindah.putExtra("ejaan","A - N - G - G - U - R");
                pindah.putExtra("info","Buah Anggur memiliki biji yang ukurannya kecil di dalam daging buahnya\n" +
                        "Tanaman anggur hidup di kawasan daratan rendah.\n" +
                        "Jenis tanaman satu ini memerlukan musim kemarau. Dan agar jenis tanaman satu ini\n" +
                        "bisa tumbuh, dibutuhkanlah yang namanya sinar matahari.\n" +
                        "Dengan demikian, maka akan menghasilkan buah anggur yang lezat");
                startActivity(pindah);
            }
        });

        ImgApel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(buahActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.apel);
                pindah.putExtra("nama", "A P E L");
                pindah.putExtra("ejaan","A - P - E - L");
                pindah.putExtra("info","Apel merupakan jenis buah-buahan yang bagian luarnya biasanya berwarna merah\n" +
                        "saat sudah matang dan siap untuk dimakan.\n" +
                        "Akan tetapi, ada juga buah apel yang warnanya kuning atau hijau saja sudah matang.\n" +
                        "Pohon apel sendiri biasanya tumbuh pada daerah-daerah yang mempunyai suhu udara sejuk\n" +
                        "dan dingin. Pohonnya sendiri sekitar tiga sampai dua belas meter dan menghasilkan\n" +
                        "jenis buah yang manis. Di samping itu, Apel memiliki biji yang ukurannya kecil\n" +
                        "di dalam daging buahnya");
                startActivity(pindah);
            }
        });


        ImgBelimbing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(buahActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.belimbing);
                pindah.putExtra("nama", "B E L I M B I N G");
                pindah.putExtra("ejaan","B - E - L - I - M - B - I - N - G");
                pindah.putExtra("info","Buah belimbing mempunyai warna kuning kehijauan.\n" +
                        "Ketika sedang proses tumbuh, buahnya memiliki warna hijau. Buah ini memiliki rasa manis namun sedikit ada unsur asamnya.\n" +
                        "Adapun bentuknya sendiri seperti bintang. Saat di potong buah ini banyak mengandung vitamin C sehingga sangat baik untuk kesehatan manusia. Belimbing dengan warna kuning cerah tentu saja memiliki rasa yang manis");
                startActivity(pindah);
            }
        });



        ImgCeri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(buahActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.ceri);
                pindah.putExtra("nama", "C E R I");
                pindah.putExtra("ejaan","C - E - R - I");
                pindah.putExtra("info","Buah cherry kerap disebut sebagai jenis buah kersen.\n" +
                        "Buah cherry ini hidup di kawasan yang mempunyai iklim sedang.\n" +
                        "manfaatnya juga sangat beragam.\n" +
                        "Misalnya adalah ia bisa dijadikan sebagai antiseptik,\n" +
                        "kemudian membantu meringankan gejala flu, sakit kepala, diabetes dan juga bisa\n" +
                        "dijadikan sebagai bahan anti peradangan");
                startActivity(pindah);
            }
        });

        ImgKelapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(buahActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.ceri);
                pindah.putExtra("nama", "K E L A P A");
                pindah.putExtra("ejaan","K - E - L - A - P - A");
                pindah.putExtra("info","Buah kelapa adalah buah tropis yang dihasilkan dari tanaman Cocos nucifera, termasuk dalam keluarga Arecaceae atau palem-paleman. Buah kelapa berbentuk bulat pada beberapa sisinya agak menyudut, berukuran kira-kira sebesar kepala manusia. Warna buah kelapa ada yang hijau dan ada yang kuning tergantung varietasnya.\n" +
                        "\n" +
                        "Daging buah kelapa terbungkus kulit bagian luar berupa serabut yang tebal dan bagian dalam berupa kayu keras atau tempurung. Tempurung kelapa membungkus daging buah berwarna putih bersih dan air buah. Air yang terdapat dalam buah berwarna bening sedikit keruh, rasanya manis menyegarkan.");
                startActivity(pindah);
            }
        });


        ImgMannga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(buahActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.mangga);
                pindah.putExtra("nama", "M A N G G A");
                pindah.putExtra("ejaan","M - A - N - G - G - A");
                pindah.putExtra("info","Buah mangga adalah salah satu jenis buah yang cukup favorit di Indonesia.\n" +
                        "Hal itu dikarenakan buah mangga memiliki rasa yang manis\n" +
                        "dan juga daging yang cukup tebal. Bentuk dan juga tingkatan rasa manis dari buah mangga\n" +
                        "cukup berbeda, tergantung dari jenis mangga apa.\n" +
                        "Karena buah mangga memiliki beberapa jenis.\n" +
                        "Di Indonesia ada beberapa jenis buah mangga yang amat terkenal dengan\n" +
                        "rasanya yang lezat, misalnya adalah mangga ponding, mangga arum manis, mangga madu,\n" +
                        "simanalagi, dan beberapa jenis yang lainnya");
                startActivity(pindah);
            }
        });


        ImgMelon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(buahActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.melon);
                pindah.putExtra("nama", "M E L O N");
                pindah.putExtra("ejaan","M - E - L - O - N");
                pindah.putExtra("info","Melon adalah jenis buah dan juga tanaman yang cukup menarik dan banyak diminati.\n" +
                        "Buah melon masuk ke dalam jenis labu-labuan.\n" +
                        "Buah melon bisa dimakan secara langsung ataupun dijadikan campuran untuk es buah.\n" +
                        "Daging dari buah melon memiliki warna orange muda atau kuning keemasan.\n" +
                        "Meskipun Anda sering menemui daging buah melon yang warnanya putih kehijauan");
                startActivity(pindah);
            }
        });


        ImgPisang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(buahActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.pisang);
                pindah.putExtra("nama", "P I S A N G");
                pindah.putExtra("ejaan","P - I - S - A - N - G");
                pindah.putExtra("info","Buah Pisang adalah jenis buah yang cukup banyak ditemukan di sekitar masyarakat.\n" +
                        "Buah pisang memiliki bentuk yang tersusun rapi di dalam tandan dan saat ia sudah matang,\n" +
                        "warnanya yang awalnya hijau bisa berubah menjadi berwarna kuning.\n" +
                        "Akan tetapi, ada juga buah pisang yang memiliki warna hijau, merah, ungu, jingga\n" +
                        "dan juga semi hitam saat buahnya sudah matang.\n" +
                        "Di dalam buah pisang sendiri ada kandungan sumber energi atau karbohidratan juga mineral.\n" +
                        "Selain itu, juga memiliki kandungan kalium.");
                startActivity(pindah);
            }
        });


        ImgStrawberry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(buahActivity.this, DetailActivity.class );
                pindah.putExtra("status","image");
                pindah.putExtra("gambar",R.drawable.strawberry);
                pindah.putExtra("nama", "S T R A W B E R R Y");
                pindah.putExtra("ejaan","S - T - R - A - W - B - E - R - R - Y");
                pindah.putExtra("info","Strawberry adalah jenis buah yang ketika matang berwarna merah.\n" +
                        "Namun saat proses berkembang, buah strawberry mempunyai warna hijau keputihan.\n" +
                        "Buah strawberry ada yang rasanya manis dan ada yang rasanya asam manis.\n" +
                        "Biasanya, jenis buah strawberrycmudah ditemui di daerah yang iklimnya sejuk.");
                startActivity(pindah);
            }
        });


        toBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(buahActivity.this, Dashboard.class );
                startActivity(pindah);
            }
        });
    }
}
